package com.example.tbcretrofitfirstexamle

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tbcretrofitfirstexamle.api.RetrofitService
import com.example.tbcretrofitfirstexamle.models.CountryModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CountryViewModel : ViewModel() {

    private val countryList = MutableLiveData<List<CountryModel>>().apply {
        mutableListOf<List<CountryModel>>()
    }
    val _countryList: LiveData<List<CountryModel>> = countryList

    private val isLoading = MutableLiveData<Boolean>()
    val _isLoading: LiveData<Boolean> = isLoading
    fun init() {
        CoroutineScope(Dispatchers.IO).launch {
            getCountriesData()
        }
    }

    private suspend fun getCountriesData() {
        isLoading.postValue(true)
        val result = RetrofitService.retrofitService().getCountry()
        if (result.isSuccessful) {
            result.body()?.let { countryList.postValue(it) }
        } else {
            Log.d("RESPONSE", "${result.errorBody()}")
        }
        isLoading.postValue(false)
    }
}