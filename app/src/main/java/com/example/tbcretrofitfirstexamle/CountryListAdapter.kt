package com.example.tbcretrofitfirstexamle

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcretrofitfirstexamle.databinding.RowItemBinding
import com.example.tbcretrofitfirstexamle.models.CountryModel
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

class CountryListAdapter() : RecyclerView.Adapter<CountryListAdapter.MyViewHolder>() {

    var countryList = mutableListOf<CountryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = countryList.size

    inner class MyViewHolder(val binding: RowItemBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var country: CountryModel
        fun onBind() {
            country = countryList[adapterPosition]
            binding.apply {
                tvCountryName.text = country.name
                tvRegion.text = country.region
                root.setOnClickListener {
                    onItemClick?.let { it(country) }
                }
            }
            loadImage(binding.ivFlag, country.flag)
        }

        private fun loadImage(imageView: ImageView, url: String?) {
            GlideToVectorYou
                .init()
                .with(imageView.context)
                .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                .load(Uri.parse(url), imageView);
        }
    }


    fun setContent(list: MutableList<CountryModel>) {
        this.countryList.clear()
        this.countryList.addAll(list)
        notifyDataSetChanged()
    }

    fun clearContent() {
        this.countryList.clear()
        notifyDataSetChanged()
    }

    private var onItemClick: ((CountryModel) -> Unit)? = null
    fun setOnItemClickListener(listener: (CountryModel) -> Unit) {
        onItemClick = listener
    }
}