package com.example.tbcretrofitfirstexamle.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

data class CountryModel(
    val name: String? = null,
    val capital: String? = null,
    val region: String? = null,
    val area:Double? = null,
    val population: Int? = null,
    val flag: String? = null,
    val currencies: List<CurrencyModel>,
    val languages:List<LanguageModel>
):Serializable
