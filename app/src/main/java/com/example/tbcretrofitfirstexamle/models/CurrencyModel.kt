package com.example.tbcretrofitfirstexamle.models

data class CurrencyModel(
    val name: String? = null,
    val symbol: String? = null
)