package com.example.tbcretrofitfirstexamle

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.tbcretrofitfirstexamle.Constants.BUNDLE_KEY
import com.example.tbcretrofitfirstexamle.Constants.REQUEST_CODE
import com.example.tbcretrofitfirstexamle.databinding.FragmentDetailBinding
import com.example.tbcretrofitfirstexamle.models.CountryModel
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

class DetailFragment : Fragment() {
    private var _binding: FragmentDetailBinding? = null
    private val binding: FragmentDetailBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(layoutInflater, container, false)
        init()
        return _binding?.root
    }

    private fun init() {
        setData()
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setData() {
        setFragmentResultListener(REQUEST_CODE) { _, bundle ->
            val country: CountryModel = bundle.getSerializable(BUNDLE_KEY) as CountryModel
            binding.apply {
                tvCountryName.text = country.name
                tvArea.text = country.area.toString()
                tvCapital.text = country.capital
                tvCurrency.text = country.currencies[0].name
                tvPopulation.text = country.population.toString()
            }
            loadImage(binding.ivFlag, country.flag)
        }
    }

    private fun loadImage(imageView: ImageView, url: String?) {
        GlideToVectorYou
            .init()
            .with(imageView.context)
            .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
            .load(Uri.parse(url), imageView);
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}