package com.example.tbcretrofitfirstexamle.api

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitService {
    private const val BASE_URL = "https://restcountries.eu"

    fun retrofitService(): CountryApi {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create()).build()
            .create(CountryApi::class.java)
    }
}