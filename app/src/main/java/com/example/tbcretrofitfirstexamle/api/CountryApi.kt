package com.example.tbcretrofitfirstexamle.api

import com.example.tbcretrofitfirstexamle.models.CountryModel
import retrofit2.Response
import retrofit2.http.GET

interface CountryApi {
    @GET("/rest/v2/all")
    suspend fun getCountry(): Response<List<CountryModel>>
}