package com.example.tbcretrofitfirstexamle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcretrofitfirstexamle.Constants.BUNDLE_KEY
import com.example.tbcretrofitfirstexamle.Constants.REQUEST_CODE
import com.example.tbcretrofitfirstexamle.databinding.FragmentMainBinding

class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding get() = _binding!!

    private val countryAdapter: CountryListAdapter by lazy { CountryListAdapter() }
    private val countryViewModel: CountryViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        init()
        return _binding?.root
    }

    private fun init() {
        countryViewModel.init()
        setRecycleView()
        observeData()
        binding.swipeRefreshLayout.setOnRefreshListener {
            countryAdapter.clearContent()
            countryViewModel.init()
        }
        countryAdapter.setOnItemClickListener {
            setFragmentResult(REQUEST_CODE, bundleOf(BUNDLE_KEY to it))
            findNavController().navigate(R.id.action_mainFragment_to_detailFragment)
        }
    }

    private fun setRecycleView() {
        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = countryAdapter
        }
    }

    private fun observeData() {
        countryViewModel._isLoading.observe(viewLifecycleOwner, {
            binding.swipeRefreshLayout.isRefreshing = it
        })
        countryViewModel._countryList.observe(viewLifecycleOwner, {
            countryAdapter.setContent(it.toMutableList())
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}