package com.example.tbcretrofitfirstexamle

object Constants {
    const val REQUEST_CODE = "request_code"
    const val BUNDLE_KEY = "bundle_key"
}